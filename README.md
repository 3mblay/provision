# Provision

On a new machine `mymachine`

1. Remote login to `mymachine`
  
    ``` bash
    ssh root@mymachine
    ```

1. Enable unnattended upgrades 
  
    ``` bash
    sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
    ```

1. Upgrade the system

    ```bash
    sudo apt update
    sudo apt upgrade --yes
    sudo apt autoremove
    sudo /sbin/reboot # if necessary
    ```

1. Create a new user
    
    ``` bash
    sudo useradd -s /bin/bash -d /home/christina/ -m -G sudo christina
    sudo passwd christina
    ```

1. Install [dotfiles](https://gitlab.com/3mblay/dotfiles)

    ``` bash
    su christina
    ```

1. Copy your key for password-less access

    ``` bash
    ssh-copy-id -i /home/paul/.ssh/id_ed25519.pub christina@mymachine
    vim ~/.ssh/config # to add a new hostname configuration
    ```